import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redaxcounter/reduser/app_reducer.dart';
import 'package:redaxcounter/reduser/counter_reducer.dart';
import 'package:redaxcounter/states/app_state.dart';
import 'package:redaxcounter/states/counter_state.dart';
import 'package:redux/redux.dart';
import 'my_app_viewmodel.dart';

void main() => runApp(Application());

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyApp(),
      theme: ThemeData.dark(),
    );
  }
}

class MyApp extends StatelessWidget {
  Store appStore = Store<AppState>(
  appReducer,
  initialState: AppState.loading(),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: Store<CounterState>(
        counterReducer,
        initialState: CounterState.init(),
      ),
      child: StoreConnector<CounterState, MyAppViewModel>(
        converter: (store) => MyAppViewModel.loading(store),
        builder: (context, counter) {
          return Scaffold(
            appBar: AppBar(
              title: Text('Test'),
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      child: Text('+'),
                      onPressed: counter.increment,
                    ),
                    Text(
                      '${counter.counter}',
                      style:
                          TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                    ),
                    RaisedButton(
                      child: Text('-'),
                      onPressed: counter.decrement,
                    ),
                  ],
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
