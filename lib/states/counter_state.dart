import 'package:flutter/material.dart';

class CounterState {
  final int counter;

  CounterState(this.counter);

  factory CounterState.init() => CounterState(0);
}
