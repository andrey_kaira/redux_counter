import 'package:flutter/cupertino.dart';
import 'package:redaxcounter/states/counter_state.dart';


class AppState {
  final CounterState counterState;

  AppState({
    this.counterState,
  });

  factory AppState.loading() => AppState(counterState: CounterState.init());
}
