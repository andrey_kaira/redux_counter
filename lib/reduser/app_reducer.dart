import 'package:redaxcounter/reduser/counter_reducer.dart';
import 'package:redaxcounter/states/app_state.dart';

AppState appReducer(AppState state, dynamic action) {
  return AppState(
    counterState: counterReducer(state.counterState, action),
  );
}
