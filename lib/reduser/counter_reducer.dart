import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:redaxcounter/actions/actions.dart';
import 'package:redaxcounter/states/counter_state.dart';

CounterState counterReducer(CounterState prev, dynamic action) {
  if (action == Increment) {
    return CounterState(prev.counter + 1);
  }
  if (action == Decrement) {
    return CounterState(prev.counter - 1);
  }
  return prev;
}
