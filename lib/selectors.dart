import 'package:redaxcounter/actions/actions.dart';
import 'package:redaxcounter/states/counter_state.dart';
import 'package:redux/redux.dart';

class CounterSelector{
    static void Function() getIncrementFunction(Store<CounterState> store){
      return ()=> store.dispatch(Increment);
    }

    static void Function() getDecrementFunction(Store<CounterState> store){
      return ()=> store.dispatch(Decrement);
    }

    static int getCurrentCounter(Store<CounterState> store){
      return store.state.counter;
    }
 }