import 'package:redaxcounter/selectors.dart';
import 'package:redaxcounter/states/app_state.dart';
import 'package:redaxcounter/states/counter_state.dart';
import 'package:redux/redux.dart';

class MyAppViewModel {
  final void Function() increment;
  final void Function() decrement;
  final int counter;

  MyAppViewModel({
    this.counter,
    this.decrement,
    this.increment,
  });

  factory MyAppViewModel.loading(Store<CounterState> store) => MyAppViewModel(
        increment: CounterSelector.getIncrementFunction(store),
        decrement: CounterSelector.getDecrementFunction(store),
        counter: CounterSelector.getCurrentCounter(store),
      );
}
